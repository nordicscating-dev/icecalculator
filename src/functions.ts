export const jaankasvu = (
    temperature: number,
    cloudiness: number,
    windSpeed: number,
    humidity: number) => {

    return Math.round(getJaankasvu(temperature, cloudiness, windSpeed, humidity) * 100) / 100;
}

function getJaankasvu(
    temperature: number,
    cloudiness: number,
    windSpeed: number,
    humidity: number): number {

    return getLammonsiirto(temperature, cloudiness, windSpeed, humidity) / (-85);
}

function getLammonsiirto(temperature: number,
                        cloudiness: number,
                        windSpeed: number,
                        humidity: number): number {
    const aurinko: number = 10;
    const sateily: number = -309;

    const vastasateily: number = getVastasateily(getCloudinessText(cloudiness), temperature);
    const lammonjohtavuus: number = getThermalConductivity(temperature);
    const haihtuminen: number = getHaihtuminen(getHumidityText(humidity), temperature);
    
    return aurinko + sateily + vastasateily + windSpeed * (lammonjohtavuus + haihtuminen);
}

function getThermalConductivity(temperature: number): number {
    return 1.507692308 * temperature;
}

function getCloudinessText(cloudiness: number): string {
    if (cloudiness === 0) {
        console.log("cloudiness nolla !!!!!!!!!!!!!!!!!!!!!!!!");
        return 'Helmulet';
    }

    if (cloudiness === null) {
        console.log("cloudiness NULL !!!!!!!!!!!!!!!!!!!!!!!!");
        return 'Helmulet';
    }


    if (cloudiness < 2) return 'Helt klart';
    else if (cloudiness < 7) return 'Halvmulet';
    return 'Helmulet'
}

function getHumidityText(humidity: number): string {
    if (humidity < 70) return 'Låg';
    else if (humidity < 90) return 'Mellan';
    return 'Hög';
}

function getVastasateily(cloudinessStr: string, temperature: number): number {
    if (cloudinessStr == "Helt klart")
        return 3.84615 * temperature + 246;
    else if (cloudinessStr == "Halvmulet")
        return 4.346154 * temperature + 278;
    else if (cloudinessStr == "Helmulet")
        return 4.807692 * temperature + 310;

    console.log("error getVastasateily " + cloudinessStr);

    return 0;
}

function getHaihtuminen(humidityStr: string, temperature: number): number {
    if (humidityStr == "Hög")
        return 0.0195 * temperature ** 2 + 0.842 * temperature;
    if (humidityStr == "Mellan")
        return 0.0158 * temperature ** 2 + 0.682 * temperature - 2.4;
    if (humidityStr == "Låg")
        return 0.01075 * temperature ** 2 + 0.5 * temperature - 4.7;

    console.log("error getHaihtuminen");

    return 0;
}
